# Enrol users into Moodle course

Enrol users into the Moodle course with the [local_mass_enrol page](https://moodle.org/plugins/local_mass_enroll).
Note that due to compatibility of the plugin and Moodle 4.0, you will need to first 
go to the course page (which in the URL includes the courseid) and then replace the url with
"<your-root>/local/mass_enroll/mass_enroll.php?id=<course-id>", e.g. "localhost/local/mass_enroll/mass_enroll.php?id=4".

Enrol first teachers (with "teacher_enrol.csv"), and then students (with "student_enrol.csv"). Make sure to
select the appropriate role when enrolling. And select that the "First column contains" " Email address".
