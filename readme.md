# CSV to MBZ pipeline
This notebook contains the process for transforming CSV files into a Moodle backup file (MBZ).

Each file represents a step in the data processing pipeline. The order of steps is fixed and indicated by the number the filename starts with.

## Use Cases
* Teaching with Moodle 2016: The branch "twm" transforms CSV files from the "Teaching with Moodle" course 2016. See the [twm dataset](https://gitlab.com/iug-research/moodle-learning-analytics/dataset-twm).
* Brueckenkurs Informatik 2022: The branch "bki" transforms CSV files from the course "Brückenkurs Informatik" from 2022. See the [bki dataset](https://gitlab.com/iug-research/moodle-learning-analytics/dataset-bki).

