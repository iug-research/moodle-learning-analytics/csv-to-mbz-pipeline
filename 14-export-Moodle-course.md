# Export Moodle course

During the process until now the following steps have been done to prepare a Moodle course:
* Set up a clean Moodle system with Docker
* Create a new, empty course with no enrolled users
* Recreate original course structure
* Enrolled users into the course
* Imported grades

What is needed for the dropout prediction model is user activity.
Activity logs as provided in the data obtained from Moodle HQ needs to be imported into 
Moodle. However, Moodle does not provide a functionality for importing logs in CSV format.
It is possible, to "Include course logs" in backup files though.

Therefore, the next step is to create a backup file of the course in its current state. See
[Moodle documentation: Course backup](https://docs.moodle.org/400/en/Course_backup) for instructions.
Make sure to "Include course logs" when creating the backup. It is not necessary to include 
some of the other course properties, e.g. calendar events or blocks. 

You can also use the backup file provided in the data folder of this repository for proceeding.
