# Import users into Moodle system

Import the users into your Moodle system via the "user_upload.csv" file, 
as described at [Moodle documentation: Bulk upload users](https://docs.moodle.org/400/en/Bulk_upload_users). 
Note that this is the file in the "data" folder. 