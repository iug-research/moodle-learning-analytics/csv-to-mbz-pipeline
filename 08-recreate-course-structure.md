# Recreate course structure
Replicants of this experiment now have two options:

a) One can recreate the course structure manually with the strategy detailed in the accompanying
master thesis (based on the table created in the previous notebook), or with another strategy.

b) The author of this repo has recreated the course structure as detailed in the accompanying 
master thesis. One can also import this structure, saved as an MBZ file in the "data" folder of this repo.
For that, in the empty Moodle course created earlier, chose "More" and "Course reuse" and "Restore".
Upload the backup MBZ file. Chose "Restore into this course" and "Delete the contents of this course and then restore".
In step 4, "schema", set "Overwrite course configuration" to "Yes". Overwrite the course start.
Do not overwrite the course name and short name.

Next, you need to download the recreated course from Moodle as an MBZ, because course 
elements will have received new identities (contextinstanceids, contextids, objectids).
Save the MBZ in "data". 