# Import Moodle course
Finally, you can import the Moodle course backup file created in this repo, 
by using Moodle's course restore function. See [Moodle documentation: Course restore](https://docs.moodle.org/400/en/Course_restore)
for instructions.

It is cleaner to create a new empty course in which to import the 
backup file, than to overwrite the course structure.

When importing, import all data possible.
Also chose to "Overwrite course configuration", but deselect course short name and name.
