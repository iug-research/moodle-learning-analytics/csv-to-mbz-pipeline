# Import grades

Students' grades in form of the gradebook CSV file created previously can be imported into 
the gradebook of the Moodle course. See [Moodle documentation: Grade import](https://docs.moodle.org/400/en/Grade_import)
for details. Thanks to descriptive grade column names, mapping the correct grade 
to the correct gradable module should be feasible.

Make sure to select "username" for identifying the user.