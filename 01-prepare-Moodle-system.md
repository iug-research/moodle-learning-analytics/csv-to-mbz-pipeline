# Prepare Moodle system

The first step in the pipeline for augmented data import into a Moodle system
is to prepare a clean Moodle system for further steps. 

## Set up Moodle system

Set up a local Moodle system for testing using 
[Docker Moodle](https://gitlab.rz.htw-berlin.de/fair-enough/docker-moodle)
(see the instructions for setting up Moodle in the readme of that repository).
For this experiment, Moodle 4.2 has been used.

Log in to the Moodle system as an admin.

## Install necessary plugin
Install the Plugin [local_mass_enroll](https://moodle.org/plugins/local_mass_enroll) as detailed on the
plugin page. This allows to later enroll many users at once into a course, via CSV files.
For this experiment, version 4.0.0 of the plugin has been used (latest plugin at this point).

## Create empty course
Create a new, empty course with default settings. 

Delete the Announcements forum, and unenrol the admin user.
